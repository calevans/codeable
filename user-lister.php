<?php
/**
 * Plugin Name:     User Lister
 * Plugin URI:      https://blog.calevans.com
 * Description:     Short tag that displays a list of users for the admin
 * Author:          Cal Evans
 * Author URI:      https://blog.calevans.com
 * Text Domain:     user-lister
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         UserLister
 */


namespace UserLister;

require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

$bootstrap = new Bootstrap();
