var codeable_users = new Array();
jQuery( document ).ready( function() {
	jQuery.ajax( {
			url: wpApiSettings.root + 'wp/v2/users/?context=edit&per_page=100',
			method: 'GET',
			beforeSend: function ( xhr ) {
					xhr.setRequestHeader( 'X-WP-Nonce', wpApiSettings.nonce );
			}
	} ).done( function ( response ) {
			response.forEach(element => {
				codeable_users.push(
					{
						username: element.username,
						display_name: element.name,
						role: element.roles[0]
					}
				);
			});
			createTable(codeable_users);
		}
	)
	}
);

function createTable(codeable_users) {

	var table = new Tabulator("#example-table", {
		data: codeable_users,			//load row data from array
		layout: "fitColumns",			//fit columns to width of table
		tooltips: true,						//show tool tips on cells
		addRowPos: "top",					//when adding a new row, add it to the top of the table
		history: true,						//allow undo and redo actions on the table
		pagination: "local",			//paginate the data
		paginationSize: 10,				//allow 7 rows per page of data
		movableColumns: false,		//allow column order to be changed
		resizableRows: false,			//allow row order to be changed
		initialSort:[							//set the initial sort order of the data
			{column:"username", dir:"asc"},
		],
		responsiveLayout:"hide",	//hide columns that dont fit on the table
		columns:[								 //define the table columns
			{title: wpApiSettings.headers.username, field:"username"},
			{title: wpApiSettings.headers.display_name, field:"display_name" },
			{title: wpApiSettings.headers.role, field:"role",headerFilter:true}
		],
	});
}

