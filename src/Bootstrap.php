<?php
namespace UserLister;

class Bootstrap
{

	public function __construct() {
		add_shortcode( 'user_lister', [$this, 'enqueue_user_lister'] );
	}

	public function enqueue_user_lister() {
		if ( ! current_user_can( 'manage_options' ) ) {
			return __(
        "I'm sorry, only administrators can see the user list.",
        'user-lister'
      );
		}

		add_action( 'wp_footer', [$this,'set_wpApiSettings'] );
		wp_enqueue_script(
			'tabulator',
			'https://unpkg.com/tabulator-tables@4.2.3/dist/js/tabulator.min.js',
			['jquery']
		);
		wp_enqueue_script(
			'user_lister',
			plugins_url('user-lister') . '/js/user-lister.js',
			['tabulator']
		);

		wp_enqueue_style(
			'tabulator',
			'https://unpkg.com/tabulator-tables@4.2.3/dist/css/tabulator.min.css'
		);
		return '<div id="example-table"></div>';
	}


	public function set_wpApiSettings() {
		wp_localize_script(
			'user_lister',
			'wpApiSettings',
			[
				'root' => esc_url_raw( rest_url() ),
				'nonce' => wp_create_nonce( 'wp_rest' ),
				'headers' => [
					'display_name' => __( 'Display Name', 'user-lister' ),
					'username' => __( 'User Name', 'user-lister' ),
					'role' => __( 'Role', 'user-lister' )
				]
			]
		);
	}
}
