# User Lister

Contributors: Cal Evans

Tags: admin, user management

Requires at least: 4.5

Tested up to: 5.1.1

Stable tag: 1.0.0

License: MIT

License URI: https://opensource.org/licenses/MIT

This plugin create a short code that can be included on any page. If the page is viewed by an administrator, it will present a paginated list of the users in the system. Otherwsie, ti will present a message.

## Instructions for installation

1. Install and activate the plugin.
1. Make a page or a post to hold the lister.
1. add the shortcode `[user_lister]` where you want the lister to appear.

## Changelog

### 1.0
* First working copy
